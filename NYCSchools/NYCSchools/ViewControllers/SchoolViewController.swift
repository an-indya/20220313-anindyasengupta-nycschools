//
//  ViewController.swift
//  NYCSchools
//
//  Created by Anindya Sengupta on 3/13/22.
//

import UIKit
import CoreData

class SchoolViewController: UIViewController, SegueHandler {

    @IBOutlet weak var tableView: UITableView!
    private let refreshControl = UIRefreshControl()
    
    @IBAction func sortDescriptorChanged(_ sender: Any) {
        if let segmentedControl = sender as? UISegmentedControl {
            setupTableView(with: segmentedControl.selectedSegmentIndex)
        }
    }
    
    enum SegueIdentifier: String {
        case showSchool = "showSchool"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        populateSchools()
        tableView.delegate = self
        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(populateSchools), for: .valueChanged)
        
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.showAlert),
            name: Notification.Name(rawValue: Keys.networkNotificationKey),
            object: nil)
    }
    
    @objc func populateSchools () {
        let isFirstTime = UserDefaults.standard.bool(forKey: Keys.kIsFirstLaunch)
        if !isFirstTime {
            SchoolDataManager.clearDatabase { clean in
                if !clean { return }
                SchoolNetworkManager.getAllSchools {
                    DispatchQueue.main.async {
                        self.setupTableView(with: 0) //Initially calling sort descriptor zero, which is alphabatical
                        
                    }
                }
            }
        } else {
            SchoolNetworkManager.getAllSchools {
                DispatchQueue.main.async {
                    self.setupTableView(with: 0) //Initially calling sort descriptor zero, which is alphabatical
                    
                }
            }
        }       
        self.refreshControl.endRefreshing()
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? SchoolDetailViewController,
            let school = sender as? School {
                destination.school = school
        }
    }
    
    // MARK: Private

    fileprivate var dataSource: TableViewDataSource<SchoolViewController>!
    let sortDescriptors = ["name", "sat_writing", "sat_critical_reading", "sat_math"]
    
    fileprivate func setupTableView(with sortDescriptorIndex: Int) {        
        let managedObjectContext = CoreDataManager.shared.managedObjectContext
        let request = School.sortedFetchRequest
        request.sortDescriptors = [NSSortDescriptor(key: sortDescriptors[sortDescriptorIndex], ascending: sortDescriptorIndex == 0)]
        
        
        request.returnsObjectsAsFaults = false
        request.fetchBatchSize = 10
        let frc = NSFetchedResultsController(fetchRequest: request, managedObjectContext: managedObjectContext!, sectionNameKeyPath: nil, cacheName: nil)
        guard let tv = tableView else { fatalError("must have table view") }
        dataSource = TableViewDataSource(tableView: tv, cellIdentifier: "schoolCell", fetchedResultsController: frc, delegate: self)
    }
    
    @objc func showAlert () {
        DispatchQueue.main.async {
            let alertAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            AlertManager.showAlert(message: "The Internet connection appears to be offline. Please try again when connection resumes.", title: "Error!", in: self.navigationController!, with: [alertAction])
        }
    }

}


extension SchoolViewController: TableViewDataSourceDelegate {
    func configure(_ cell: SchoolTableViewCell, for object: School) {
        cell.configure(for: object)
    }
}


extension SchoolViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let school = dataSource.objectAtIndexPath(indexPath)
        performSegue(withIdentifier: SegueIdentifier.showSchool.rawValue, sender: school)
    }
}
