//
//  ViewController.swift
//  NYCSchools
//
//  Created by Anindya Sengupta on 3/13/22.
//

import UIKit
import CoreData

class SchoolDetailViewController: UIViewController {
    
    var school: School?
    
    @IBOutlet weak var schoolName: UILabel!
    @IBOutlet weak var schoolDetails: UILabel!
    @IBOutlet weak var sat_writing: UILabel!
    @IBOutlet weak var sat_math: UILabel!
    @IBOutlet weak var sat_reading: UILabel!
    @IBOutlet weak var phone: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        populateDetails()
    }
    
    ///Given more time, I would implement this in a tableview and setup sections and custom cells to make the whole view scrollable and nice looking
    func populateDetails() {
        schoolName.text = school?.name
        schoolDetails.text = school?.overview
        phone.text = "Contact: " + String(school?.phone ?? "")
        
        self.sat_reading.text = "SAT Reading: " + String(school?.sat_critical_reading ?? 0)
        self.sat_writing.text = "SAT Writing: " + String(school?.sat_writing ?? 0)
        self.sat_math.text = "SAT Maths: " + String(school?.sat_math ?? 0)
    }
}
