//
//  SchoolTableViewCell.swift
//  NYCSchools
//
//  Created by Anindya Sengupta on 3/15/22.
//

import UIKit

class SchoolTableViewCell: UITableViewCell {
    @IBOutlet weak var schoolName: UILabel!
}

extension SchoolTableViewCell {
    func configure(for school: School) {
        schoolName.text = school.name        
    }
}
