//
//  SchoolDataManager.swift
//  NYCSchools
//
//  Created by Anindya Sengupta on 3/15/22.
//

import Foundation
import CoreData

struct SATScores {
    var dbn: String
    var SATwriting: Int
    var SATCriticalReading: Int
    var SATMaths: Int
    var totalTestTakers: Int
}

final class SchoolDataManager {
    
    class func clearDatabase (completion: @escaping (_ clean: Bool) -> Void) {
        if let managedObjectContext = CoreDataManager.shared.managedObjectContext {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: School.entityName)
            do {
                var schools = try managedObjectContext.fetch(fetchRequest) as! [School]
                managedObjectContext.performAndWait {
                    schools.removeAll()
                    completion(true)
                }
                
            } catch {
                print("Fetching Failed")
                completion(false)
            }
        }
    }

    class func insertSchool (with dbn: String, name: String, location: String, overview: String?, phone: String, completion: @escaping (School?) -> Void){

        if let managedObjectContext = CoreDataManager.shared.managedObjectContext {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: School.entityName)            
            let predicate = NSPredicate(format: "%K == %@", "dbn", dbn)
            fetchRequest.predicate = predicate
            do {
                let schools = try managedObjectContext.fetch(fetchRequest) as! [School]
                if schools.count == 0 {

                    managedObjectContext.performAndWait {
                        let school = School.insert(into: managedObjectContext, dbn: dbn, name: name, location: location, overview: overview, phone: phone)
                        completion(school)
                    }
                } else {
                    completion(schools.first)
                }
            } catch {
                print("Fetching Failed")
                completion(nil)
            }
        }
    }

    class func updateSchool (dbn: String, with scores: SATScores){
        if let managedObjectContext = CoreDataManager.shared.managedObjectContext {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: School.entityName)
            let predicate = NSPredicate(format: "%K == %@", "dbn", dbn)
            fetchRequest.predicate = predicate
            do {
                if let schools = try managedObjectContext.fetch(fetchRequest) as? [School] {
                    if let school = schools.first {
                        managedObjectContext.performAndWait {
                            school.sat_math = Int32(scores.SATMaths)
                            school.sat_critical_reading = Int32(scores.SATCriticalReading)
                            school.sat_writing = Int32(scores.SATwriting)
                        }
                    }
                }
            } catch {
                print("Fetching Failed")
            }
        }
    }

    class func fetchSchool(with dbn: String, completion: (School?)-> Void) {
        if let managedObjectContext = CoreDataManager.shared.managedObjectContext {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: School.entityName)
            let predicate = NSPredicate(format: "%K == %@", "dbn", dbn)
            fetchRequest.predicate = predicate
            do {
                if let schools = try managedObjectContext.fetch(fetchRequest) as? [School],
                    let school = schools.first {
                    completion(school)
                }
            } catch {
                print("Fetching Failed")
                completion(nil)
            }
        }
    }
    
}
