//
//  NetworkManager.swift
//  NYCSchools
//
//  Created by Anindya Sengupta on 3/15/22.
//

import Foundation
import CoreLocation

class SchoolNetworkManager: NSObject {

    class func getAllSchools(completion: @escaping () -> Void) {
        var urlComponents = URLComponents()
        let url = URL(string: URLs.BaseURL)!
        urlComponents.scheme = url.scheme
        urlComponents.host = url.host
        urlComponents.path = Endpoints.schools

        if let url = urlComponents.url {
            let session = URLSession.shared
            var request = URLRequest(url: url)
            request.httpMethod = methodName.GET.rawValue

            let task = session.dataTask(with: request) {
                (data, response, error) in
                guard let _:Data = data, let _:URLResponse = response, error == nil else {
                    print("error: \(String(describing: error?.localizedDescription))")
                    NotificationCenter.default.post(name: Notification.Name(rawValue: Keys.networkNotificationKey), object: error?.localizedDescription)
                    completion()
                    return
                }
                do {
                    let responseJson  = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                        parse(json: responseJson, completion: { _ in
                            completion()
                            UserDefaults.standard.set(false, forKey: Keys.kIsFirstLaunch)
                            DispatchQueue.main.async {
                                getSATScores ()
                            }                            
                        })
                } catch {
                    print("Error: \(error.localizedDescription)")
                    completion()
                }
            }
            task.resume()
        }
    }


    class func getSATScores () {
        var urlComponents = URLComponents()
        let url = URL(string: URLs.BaseURL)!
        urlComponents.scheme = url.scheme
        urlComponents.host = url.host
        urlComponents.path = Endpoints.satResults

        if let url = urlComponents.url {
            let session = URLSession.shared
            var request = URLRequest(url: url)
            request.httpMethod = methodName.GET.rawValue

            let task = session.dataTask(with: request) {
                (data, response, error) in
                guard let _:Data = data, let _:URLResponse = response, error == nil else {
                    return
                }
                do {
                    let responseJson  = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                    parseSATScores(json: responseJson)
                } catch {
                    print("Error: \(error.localizedDescription)")
                }
            }
            task.resume()
        }
    }



    private class func parse(json: Any, completion: @escaping ([School]?) -> Void) {
        if let schoolDictArr = json as? [[String: Any]] {
            var schools = [School]()
            for (idx, school) in schoolDictArr.enumerated() {
                
                if let name = school["school_name"] as? String,
                   let overview = school["overview_paragraph"] as? String,
                   let dbn = school["dbn"] as? String,
                   let location = school["location"] as? String,
                   let phone = school["phone_number"]  as? String {
                    
                    DispatchQueue.main.async {                        
                        SchoolDataManager.insertSchool(with: dbn, name: name, location: location, overview: overview, phone: phone) { school in                            
                            if let school = school {
                                schools.append(school)
                                if idx == schools.count - 1 {
                                    completion(schools)
                                }
                            }
                        }
                    }
                } else {
                    completion(nil)
                }
            }
        }
    }
    
    private class func parseSATScores (json: Any) {
        if let SATArr = json as? [[String: Any]] {
            for scores in SATArr {
                if let dbn = scores["dbn"] as? String,
                   let totalTestTakers = Int(scores["num_of_sat_test_takers"] as? String ?? "0"),
                   let criticalReadingAvgScore = Int(scores["sat_critical_reading_avg_score"] as? String ?? "0"),
                   let mathAvgScore = Int(scores["sat_math_avg_score"]  as? String ?? "0"),
                   let writingAvgScore = Int(scores["sat_writing_avg_score"] as? String ?? "0") {
                        let satScore = SATScores(dbn: dbn, SATwriting: writingAvgScore, SATCriticalReading: criticalReadingAvgScore, SATMaths: mathAvgScore, totalTestTakers: totalTestTakers)
                    SchoolDataManager.updateSchool(dbn: dbn, with: satScore)
                }
            }
        }
    }
    
    
    
    
//
//    private class func downloadImages (with mealImages: [MealImage]) {
//        for mealImage in mealImages {
//            Downloader.load(url: URL(string: mealImage.thumbnailURL)!, completion: { data in
//                if let data = data {
//                    DispatchQueue.main.async {
//                        MealDataManager.updateMeal(id: mealImage.mealId, with: data)
//                    }
//                }
//            })
//        }
//
//    }
//}
//
//
//
//
//class Downloader {
//    class func load(url: URL, completion: @escaping (Data?) -> Void) {
//        let sessionConfig = URLSessionConfiguration.default
//        let session = URLSession(configuration: sessionConfig)
//        var request = URLRequest(url: url, cachePolicy: .returnCacheDataElseLoad)
//        request.httpMethod = "GET"
//        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
//            if let tempLocalUrl = tempLocalUrl, error == nil {
//                // Success
//                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
//                    print("Success: \(statusCode)")
//                }
//
//                do {
//                    let data = try Data(contentsOf: tempLocalUrl)
//                    completion(data)
//                } catch {
//                    print("error converting data file \(error.localizedDescription)")
//                    completion(nil)
//                }
//            } else {
//                print("Failure: \(String(describing: error?.localizedDescription))")
//                completion(nil)
//            }
//        }
//        task.resume()
//    }
}
