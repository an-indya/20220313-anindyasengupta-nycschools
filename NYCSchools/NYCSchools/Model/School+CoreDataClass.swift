//
//  School+CoreDataClass.swift
//  NYCSchools
//
//  Created by Anindya Sengupta on 3/15/22.
//
//

import Foundation
import CoreData


final class School: NSManagedObject {

    static func insert(into context: NSManagedObjectContext, dbn: String, name: String, location: String?, overview: String?, phone: String?) -> School {
        let school: School = context.insertObject()
        school.name = name
        school.dbn = dbn
        school.location = location
        school.overview = overview
        school.phone = phone
        return school
    }
}

extension School: Managed {
    static var defaultSortDescriptors: [NSSortDescriptor] {
        return [NSSortDescriptor(key: "name", ascending: true)]
    }
    
    static var SATMathSortDescriptor: [NSSortDescriptor] {
        return [NSSortDescriptor(key: "sat_math", ascending: true)]
    }
}

