//
//  Constants.swift
//
//  Created by Anindya Sengupta on 3/15/22.
//

import Foundation

enum methodName: String {
    case GET
    case POST
    case PUT
}

struct URLs {
    static let BaseURL = "https://data.cityofnewyork.us"
}

struct Endpoints {
    static let schools = "/resource/s3k6-pzi2.json"
    static let satResults = "/resource/f9bf-2cp4.json"
}

struct Keys {
    static let kIsFirstLaunch = "kIsFirstLaunch"
    static let networkNotificationKey = "com.nycschools.networkNotAvailableKey"
}

var documentsDirectory : String {
    let path = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
    let documentDirectoryPath:String = path[0]
    return documentDirectoryPath
}
