//
//  AppDelegate.swift
//  NYCSchools
//
//  Created by Anindya Sengupta on 3/13/22.
//

import UIKit
import CoreData

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var persistentContainer: NSPersistentContainer!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        //Setting up persistent store
        CoreDataManager.shared.createPersistentContainer { container in
            self.persistentContainer = container
            CoreDataManager.shared.managedObjectContext = container.viewContext
        }
        //print(documentsDirectory)
        //Marking the first launch
        UserDefaults.standard.set(true, forKey: Keys.kIsFirstLaunch)
        return true
    }
}

