//
//  NetworkManagerTests.swift
//  NYCSchoolsTests
//
//  Created by Anindya Sengupta on 4/5/22.
//

import XCTest
@testable import NYCSchools

class NetworkManagerTests: XCTestCase {
    
    class MockNetworkManager: SchoolNetworkManager {
        override func 
      }

    func testAddAsync() {
        let expectation = expectationWithDescription("the add method callback was called with the correct value")
        let networkAdder = MockNetworkAdder()
        networkAdder.add(8, callbackWithSum: { sum in
            XCTAssertEqual(sum, 13)
            expectation.fulfill()
        })
        waitForExpectationsWithTimeout(1, handler: nil)
      }

}
